<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forgot Password</title>
    <link rel="stylesheet" href="login.css">
</head>
<body class="gray">
<div class="container">
    <div class="row">
        <div class="login-wrap">
            <h2 class="account-login">Reset Password</h2>
            <form class="" action="index.html" method="post">
                <div class="form-group">
                    <label for="">Email <a href="login.php" class="push-right btn-blue">Login</a></label>
                    <input type="text" name="" class="form-control" value="">
                </div>

                <div class="btn-wrap">
                    <button type="button" class="btn btn-dark btn-login" name="button">Reset Password</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
