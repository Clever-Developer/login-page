<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Login</title>
  <link rel="stylesheet" href="login.css">
</head>
<body class="gray">
  <div class="container">
    <div class="row">
      <div class="login-wrap">
        <h2 class="account-login">Account Login</h2>
        <form class="" action="index.html" method="post">
          <div class="form-group">
            <label for="">Username <a href="register.php" class="push-right btn-blue">Create an Account</a></label>
            <input type="text" name="" class="form-control" value="">
              <small class="danger">Email is not valid</small>
          </div>
          <div class="form-group">
            <label for="">Password</label>
            <input type="password" name="" class="form-control" value="">
          </div>
          <div class="remember-me">
            <label for="remember">
              <input type="checkbox" name="" id="remember" value=""> <span>Remember me</span>
            </label>
          </div>
          <div class="forgot-password">
            <a href="forgot.php">Forgot Password?</a>
          </div>
          <div class="btn-wrap">
            <button type="button" class="btn btn-dark btn-login" name="button">Login</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</body>
</html>
